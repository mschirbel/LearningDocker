# Como é feito os deploys

Várias VMs que rodam varias aplicações

Isso pode encadear problemas, pois:
	- troca de libs pode ser um problema
	- interferencia entre aplicações
	- demora muito para iniciar
	- demora muito para fazer o deploy

# Como será:

Então, usamos containers.
Uma partição dentro do SO.

Usamos namespaces e CGroups para isolar os containers.
Com isso temos um sistema isolado, com um deploy rapido.
Isso também funciona em múltiplos ambientes

Se algum serviço ficar down, isso não afeta todo o sistema.

## Como o Docker funciona?

Há um client-server
O cliente roda as requests e envia para o servidor
O servidor roda o daemon que é usado para buildar, e baixar as imagens
Eles podem estar no mesmo host, o que não é muito bom para produção.

### Containers
Partições do Sistema Operacional

### Images
São templates para os containers

### Registries
Podem ser publicos ou privados. Como o DockerHub(publico) ou criar um private registry, assim como mostrado em: https://gitlab.com/mschirbel/NexusDockerRegistry

## Como é o processo?
Primeiro há um pull de image.

```
docker pull
docker run 
```

No host, temos o daemon do docker que vai responder os comandos que o cliente enviar

Digamos que demos um pull para uma imagem.
Do client vai para o daemon. Este, vai olhar o registry local. Se tiver uma imagem, ele roda o container. Se não, ele vai para um repositório publico que está declarado, por default, é o DockerHub.
Vai pegar a imagem e vai jogar no repositório local, assim fica disponível para futuros containers

Assim, é possível criar um container a partir de uma image.

### Namespaces
Usado para isolar processos para proteger recursos do sistema
O Docker cria uma namespace para cada container.

### CGroups
Cria partições para determinados processos.
Isso protege o host. Pois assim os containers não podem consumir muito.

### SELinux
Proteger o acesso entre containers e entre os containers com o host.

## Kubernetes
Containers são leves, e o caso deles falharem é bem comum, não se trata de um problema, mas sim de uma feature. Por exemplo, o IP de um container está sempre mudando.

As VMs, por outro lado, são pesadas, mas ficam ON por muito tempo

O kubernetes mantém a integridade do cluster, sempre subindo containers e integrando todos eles.

### Orchestration
Como fazer os containers conversarem entre si

### Schedule
Pode subir um determinado número de containers para escalar conforme a necessidade ou de acordo com as falhas.

### Isolation
Evitar que a falha de um container não afete outros.

## Kubernetes Cluster
Servidores que são administrados por um Server-Master
é usado o comando 

```
kubectl
```

para gerar os comandos de administração

### Pods
Um ou mais containers relacionados que compartilham os mesmos recursos e o mesmo IP

### Services
Persistir os IPs de Pods diferentes, assim os Pods podem ser comunicar

### Replication Controllers
Usados para fazer um scale da aplicação conforme a necessidade do tráfego. Ele assegura um número mínimo de pods.

### Persistent Volumes
Persist data. Como os containers são voláteis, pode ser que algumas configurações ou dados dentro dos containers se percam quando são iniciados novamente. Por isso é importante persistir esses dados, similar ao

```
docker volume
```

## OpenShift
Pega aquilo que o Docker e o Kubernetes usam e apresenta de uma forma amigável em uma interface fácil de administrar. Bom para Ops e Dev.
Ajuda a fazer um Pipeline de CI/CD

### Web Console
Uma console na web que permite orquestrar e escalar a aplicação
Permite controle de roles para times e users
Decide quais libs são necessárias para rodar a aplicação.

### Cloud Infrastructure
Pode ser hosteado na AWS, Azure ou até mesmo na OpenShift Cloud.

### OpenShift Routes
Expor os serviços. Para que os IPs fiquem disponíveis na Internet.

### Deployment Configurations
Varios pods, criados de uma mesma imagem. Essas configs ajudam caso seja necessario algum deploy ou modificação. Também é facil de fazer rollback

## Create MySQL Container
Podemos entrar no DockerHub para procurar a imagem:
https://hub.docker.com/

Para pegar a image do repositorio:

```
docker pull mysql:latest
```

Sendo que, ":latest" é a tag default da imagem, caso queira uma imagem específica, basta mudar para a versão disponível na página da imagem.

Podemos usar alguns parâmetros para deixar melhor esse run de imagem:

```
docker run mysql --name [nome_container] -d -it -e [VARIAVEL]
```
--name: dar um nome para identificação
-d: para deixar o processo roda em background
-it: temos um shell interativo para entrarmos no container e executar comandos
-e: para inserir variáveis de ambientes

Para acessar um container:

```
docker exec -it [container] /bin/bash
```

Podemos ver que somos root no container. E podemos conectar no MySQL:

``` 
mysql [senha]
```

Perceba que, caso esse container seja apagado, todos os dados serão apagados. Pois não persistimos os dados.

## Creating a Dockerfile

Dockerfile é uma receita para o container
Uma série de instruções para o container

O dockerfile não tem extensão!
É sempre bom criar um diretório vazio para cada Dockerfile, sendo assim o seu Working Directory

A ordem das instruções importa, como mostrado no exemplo abaixo:

```
FROM rhel7.3
LABEL description="THis is a custom httpd container image"
MAINTAINER Marcelo Schirbel <mschirbel.g@gmail.com>
RUN yum install -y httpd
EXPOSE 80
ENV LogLevel "info"
ADD http://someserver.com/filename.pdf /var/www/html
COPY ./src /var/www/html/
USER apache
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-d", "FOREGROUND"]
```
FROM diz que aquela é a nossa imagem base, sendo que, todas as modificações posteriores serão feitas em camadas superiores a nossa imagem base.

LABEL é um metadado que pode ser passado com o que quiser. É útil para boas práticas.

MAINTAINER é metadado também mas informa quem foi que escreveu isso com alguma forma de contato

RUN é algum comando que podemos rodar dentro do container

EXPOSE é quando abrimos alguma porta.

ENV cria variaveis de ambiente

ADD & COPY servem para pegar algo do host e levar para o container, como um comando scp, por exemplo.
ADD pode ser usado de hosts remotos, como URL
COPY é limitado para hosts locais(deprecado)

USER define qual user vai rodar os comandos. Boas práticas nao dizem para rodar tudo como root

ENTRYPOINT especifica o comando inicial do comando. O entrypoint default para qualquer container é /bin/sh -c

CMD define um argumento default para o container

A cada comando no Dockerfile, é criado uma layer dentro da image. Essas layers ocupam espaço, portanto, é interessante minimizar o número de layers dentro do seu Dockerfile.

```
docker build -t apache:first .
```

Podemos inspecionar a imagem:

```
docker inspect [nome da imagem]
```

Agora criaremos um container:

```
docker run --name simple-container -it [nome da imagem] /bin/bash
```

## Building Kube Resources

```
oc new-app
```
Criará uma nova aplicação, passando uma URL para algum repositório
Gerará um Pod para a aplicação

```
oc get
```
Possível pegar Pods, Images e outros recursos

```
oc describe
```
Mostra as informações do recurso

```
oc export
```
Muitos dos recursos do OpenShift são feitos em .yml, o comando salva a definição em .yml

```
oc create -f [yml file]
```
Cria o recurso

```
oc edit
```
Editar o recurso criado

```
oc delete
```
Remove algum recurso do cluster do OpenShift

```
oc exec
```
Funciona do mesmo jeito que no docker

Para ver como é instalado o Open Shift, tem um .txt aqui com as instruções.

Antes, abra um cluster local:

```
minikube start
```

Agora, para logar:

```
oc login -u [username] https://localhost:8443 --insecure-skip-tls-verify
```

Para criar um projeto:

```
oc new-project basic-kube
```

```
oc adm policy add-scc-to-user anyuid -z default
```

Para ver como está o Pod:
```
oc status -v
```

Podemos ver o Pod e mais infos:
```
oc get pods
oc describe pod [name]
```

Como acessar o Pod:
```
oc get svc
```

Com o IP do Cluster, temos acesso.

## Source-to-Image 

Primeiro, precisa providenciar um repo no git, para ter o código base da sua aplicação.

OpenShift vai pegar esse código e colocar num container básico, baseado na linguagem usada no código.

Vai usar um tipo especial de image, chamada de Builder-Image.

Vai produzir um novo container que vai rodar sua aplicação, dentro de um Pod.

Isso ajuda o dev a nao precisar se preocupar com o docker e principalmente com segurança, pois podemos tratar a infra como um código e ter um histórico de modificação usando o git.

Image Stream é um conceito que compila imagens relacionadas em um único arquivo.
Todas as diferenças de uma versão estão nesse arquivo. O que ajuda para aplicação de patches.

Depois de criar uma nova aplicação, um processo de build inicia o BuildConfig Pod.
Esse Pod é responsável por criar as iamgens no OpenShift e colocar no registry interno do Docker
SEM EFETUAR UMA CHAMADA:
Ai vem o DeploymentConfig e faz o deploy dessa imagem num Pod
O DeploymentConfig apenas reage a uma mudança na image.

```
oc login -u schirbel https://localhost:8443
```

Agora podemos criar uma nova app:
```
oc new-app [nome]:[repo do git]
```

Podemos ver os Pods:
```
oc get pods
```

Podemos ver o status dos Pods, assim que uma nova mudança ocorrer, temos um novo deploy.

Antes, precisamos do serviço:
```
oc get svc
```

Podemos ver o resultado:
```
curl [IP do svc]:8080
```

Agora vamos ver em ação:
Pegue a app e faça uma modificação.
Faça o push para o master

Agora buidamos a Image:
```
oc start-build [app]
```
*O nome tem que ser diferente, de preferencia, se for o mesmo pode dar bug :/*

## Creating Routes

É um recurso do OpenShift que pode ser usado para expor um IP de um serviço para um DNS específico, assim pode ser acessado da internet

O serviço, inicialmente, só pode se comunicar com outros pods

Assim uma rota conecta um serviço aos outros, acessável por um DNS hostname. O serviço continua ativo e o hostname torna sempre acessível.

Podemos ver o arquivo Routes.yml

Assim podemos expor:

```
oc expose svc [service]
```

## OpenShift WebConsole

É disponível no MasterNode.

https://MasterNode:8443/consol

Fica muito mais fácil para administrar os users, roles e scales nas app.

Também fica mais facil para monitorar os Pods, da pra fazer com HealthCheck
