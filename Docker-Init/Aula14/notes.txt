para iniciar um swarm:
docker swarm init

para deixar um swarm:
docker swarm leave

caso vc esteja no virtualbox, provavelmente vai dar conflito de ips, entao:
docker swarm init --advertise-addr [ip(de preferencia o 192...)]

PARA QUE HAJA UM LEADER:
sempre use o swarm init dentro do manager << isso é importante
ai depois vc cola o token nos outros
veja um exemplo em: https://rominirani.com/docker-swarm-tutorial-b67470cf8872

para criar outros nós, basta usar a docker-machine

docker-machine create [nome]
se reclamar da virtualbox, basta:
apt install virtualbox

para ver as variaveis da sua machine criada:
docker-machine env worker1

para entrar no shell da machine criada:
docker-machine ssh [nome]

para saber quais tokens vc deve usar no manager e no worker
docker swarm join-token manager
docker swarm join-toker worker

basta copiar o comando e colar dentro da machine(que vc entra usando o ssh)

as machines sao criadas em /root/.docker/machine/machines/
la tbm da pra trocar os certs

para saber o status de uma machine
docker info -> tem uma tag: Is Manager: pode ser true or false

de dentro do node manager
docker node ls - para ver o status dos outros nodess

para remover um nó do swarm, o melhor é seguir o seguinte:
entre no nó, e deixe o swarm
entre no manager e remova do nó
assim nao gera conflitos de cert na machine

sempre que for promover um node, faça de dentro do manager

por boas praticas, sempre deve ter um número ímpar de managers

