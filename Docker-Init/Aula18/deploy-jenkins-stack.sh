#!/bin/sh

DATE=$(date -I)

docker build . --no-cache -t marceloschirbel/jenkins:$DATE
docker tag marceloschirbel/jenkins:$DATE marceloschirbel/jenkins:lts 
docker tag marceloschirbel/jenkins:$DATE marceloschirbel/jenkins:latest

docker push marceloschirbel/jenkins:$DATE
docker push marceloschirbel/jenkins:latest
docker push marceloschirbel/jenkins:lts

docker stack rm jenkins
sleep 25

docker stack deploy -c docker-compose.yml jenkins
