# NexusDockerRegistry

Guia de instalação de um Private Docker Registry

## Ponto Inicial

Crie uma máquina com o nome desejado:
```
docker-machine create Nexus-Registry
```

Entre na máquina via ssh

```
docker-machine ssh Nexus-Registry
```

Crie um diretório para armazenar as configurações do nexus bem como
as suas logs:
```
cd /home/docker/
mkdir -p nexus3/
mkdir -p nexus3/data
chown -R 200 nexus3/data
```

### Criando o docker-compose.yml
Entre no diretório /nexus3

Crie um arquivo chamado docker-compose.yml

```
nexus:
  image: sonatype/nexus3:latest
  ports:
    - "8081:8081"
    - "8123:8123"
  volumes:
    - ./data:/nexus-data
```

Perceba que duas portas foram expostas, sendo que na 8081 será a interface
de web do Nexus
A outra será para configurar os pushs e pulls das imagens

O diretório data será para persistir os dados do Nexus3

### Iniciando o Nexus

Inicie o compose com:
```
docker-compose up -d
```

Caso você não tenha o compose na machine:

```
curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` > ./docker-compose
sudo mv ./docker-compose /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
```

Para ver as logs do container:
```
docker-compose logs
```

Para conectar na interface web do Nexus, use o IP da máquina que foi criada
para isso:
```
docker-machine ip Nexus-Registry
```

No Browser:
```
[Docker-Machine IP]:8081
```

### Configurando o Nexus:

Entre na interface gráfica com o usuário e senha:
admin
admin123

Clique na Engrenagem no Menu Superior
Repositórios
Create Repository
Em Recipies, selecione docker (hosted)
Dê um nome ao repositório
Marque a opção Online
Marque a opção HTTP e coloque a porta 8123
Marque a opação Enable Docker V1 API
Selecione default embaixo de BlobStore
Crie o Repositório

### Adicionando Insecure Connections na sua máquina

em /var/lib/boot2docker/profile
```
EXTRA_ARGS='
--label provider=virtualbox
--insecure-registry hostname:5000
--insecure-registry hostname:5001
--registry-mirror   http://hostname:5000
--registry-mirror   http://hostname:5001
```

ou crie a máquina virtual já com a insecure connection:

```
docker-machine create -d virtualbox --engine-insecure-registry hostname:5000  --engine-registry-mirror http://hostname:5000 n1
```

para fazer um pull sem erros de certificados:

```
docker pull --disable-content-trust
```
```
docker login -u admin -p admin123 [ip]:8123
docker pull [ip]:8123/[name]:tag
```
